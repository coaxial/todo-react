import React from 'react';

const LoginError = ({ details }) => {
  if (details) {
    return <p>error: {details}</p>;
  }
  return null;
}

const AuthForm = ({
  onUsernameChange,
  onPasswordChange,
  onLoginClick,
  onLogoutClick,
  username,
  password,
  currentUser,
  failureDetails,
  isLoggedIn,
  history,
}) => {
  if (isLoggedIn) {
    return (
      <div>
        <p>Welcome, {currentUser.displayName}!</p>
        <button onClick={onLogoutClick}>Logout</button>
      </div>
    );
  }

  return (
    <div>
      <LoginError details={failureDetails} />
      <form>
        <input
          type="text"
          name="username"
          autoComplete="username"
          value={username}
          onChange={onUsernameChange}
        />
        <input
          type="password"
          name="password"
          autoComplete="current-password"
          value={password}
          onChange={onPasswordChange}
        />
        <input
          type="submit"
          value="Login"
          onClick={e => onLoginClick(e, username, password, history)}
        />
      </form>
    </div>
  );
}

export default AuthForm;
