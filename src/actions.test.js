import { ADD_TODO, addTodo } from './actions';

describe('actions', () => {
  it('should create an action to add a todo', () => {
    const testText = 'Test todo';
    const expectedAction = {
      type: ADD_TODO,
      text: testText,
    };

    expect(addTodo(testText)).toEqual(expectedAction);
  });
});
