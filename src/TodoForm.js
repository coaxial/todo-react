// @flow

import React from 'react';

type tfType = {
  isLoggedIn: boolean,
  wipTodo: string,
  onChange: () => mixed,
  onClick: (e: SyntheticEvent<>, wipTodo: string) => mixed,
}

const TodoForm = ({ isLoggedIn, wipTodo, onChange, onClick }: tfType) => {
  if (isLoggedIn) {
    return (
      <div>
        <form>
          <input
            id="todoInput"
            type="text"
            name="newTodo"
            value={wipTodo}
            onChange={onChange}
          />
          <input
            type="submit"
            value="Add item"
            onClick={e => onClick(e, wipTodo)}
          />
        </form>
      </div>
    );
  }
  return <div />
}

export default TodoForm;
