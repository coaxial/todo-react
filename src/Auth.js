import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  charTypedPassword,
  charTypedUsername,
  clearLoginForm,
  userLogin,
  userLogout,
} from './actions';
import { isLoggedInSelector } from './selectors';
import AuthForm from './AuthForm';

const mapStateToProps = state => {
  return {
    isFetching: state.users.isFetching,
    username: state.authForm.username,
    password: state.authForm.password,
    currentUser: state.users.currentUser,
    failureDetails: state.users.lastError,
    isLoggedIn: isLoggedInSelector(state),
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onUsernameChange: e => dispatch(charTypedUsername(e.target.value)),
    onPasswordChange: e => dispatch(charTypedPassword(e.target.value)),
    onLoginClick: (e, username, password, history) => {
      e.preventDefault();
      dispatch(userLogin(username, password));
      dispatch(clearLoginForm());
      history.push('/todos/all')
    },
    onLogoutClick: e => dispatch(userLogout()),
  }
}

const Auth = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthForm));

export default Auth;
