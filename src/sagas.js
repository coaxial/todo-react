// @flow
import {
  all,
  call,
  cancel,
  fork,
  put,
  take,
  takeLatest,
} from 'redux-saga/effects';
import { fetch } from 'cross-fetch';

import {
  GET_TODOS,
  REQUEST_QUOTE,
  USER_LOGIN,
  USER_LOGIN_FAIL,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
  clearCategories,
  clearTodos,
  receiveCategories,
  receiveQuote,
  receiveTodos,
  userLoginFail,
  userLoginSuccess,
  userLogout,
} from './actions';
import type { TId, TTodosAction } from './types';

export const quoteApi = () => fetch('https://seinfeld-quotes.herokuapp.com/random').then(response => response.json())

export function* fetchQuoteSaga(): Generator<*,*,*> {
  try {
    const quote = yield call(quoteApi)
    yield put(receiveQuote(quote))
  } catch (e) {
    yield call(console.error, e);
  }
}

export function* watchFetchQuote(): Generator<*,*,*> {
  yield takeLatest(REQUEST_QUOTE, fetchQuoteSaga);
}

export const todoApi = {
  auth: (username: string, password: string) => {
    return fetch(`http://localhost:3001/users/${username}`)
      .then(response => response.json())
      .then(user => {
        if (user.password === password && user.username === username) {
          return ({
            username: user.username,
            displayName: user.displayName,
            todoIds: user.todoIds,
          });
        }
        throw new Error('Username/password invalid');
      });
  },
  getTodos:(ids: Array<TId>) => {
    const promisedTodos = ids.map(id => fetch(`http://localhost:3001/todos/${id}`)
      .then(response => response.json())
    );

    return Promise.all(promisedTodos);
  },
  getCategories: () => {
    return fetch(`http://localhost:3001/categories`)
      .then(response => response.json())
  }
};

export function* authorize(username: string, password: string): Generator<*,*,*> {
  try {
    const user = yield call(todoApi.auth, username, password);
    yield put(userLoginSuccess(user));
  } catch(e) {
    yield put(userLoginFail(e));
  }
}

export function* watchLoginFlow(): Generator<*,*,*> {
  while (true) {
    const { username, password } = yield take(USER_LOGIN)
    const loginTask = yield fork(authorize, username, password)

    const action = yield take([USER_LOGOUT, USER_LOGIN_FAIL]);
    if (action.type === USER_LOGOUT) {
      yield cancel(loginTask);
      yield put(userLogout());
      yield put(clearTodos())
      yield put(clearCategories())
    }
  }
}

export function* getTodos(action: TTodosAction): Generator<*,*,*> {
  // flow can't figure out the right case from TTodosAction
  // $FlowFixMe
  const todos = yield call(todoApi.getTodos, action.user.todoIds);
  yield put(receiveTodos(todos));
}

export function* watchTodos(): Generator<*,*,*> {
  yield takeLatest([USER_LOGIN_SUCCESS, GET_TODOS], getTodos);
}

export function* getCategories(): Generator<*,*,*> {
  const categories = yield call(todoApi.getCategories);
  yield put(receiveCategories(categories));
}

export function* watchCategories(): Generator<*,*,*> {
  yield takeLatest(USER_LOGIN_SUCCESS, getCategories);
}

export default function* rootSaga(): Generator<*,*,*> {
  yield all([
    watchFetchQuote(),
    watchLoginFlow(),
    watchTodos(),
    watchCategories(),
  ]);
}
