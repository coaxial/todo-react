import createSagaMiddlware from 'redux-saga';

import configureStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';

import { ADD_TODO, RECEIVE_QUOTE } from './actions';
import { fetchQuoteSaga } from './sagas';
import { todos } from './reducers';

describe('reducers', () => {
  describe('addTodo', () => {
    it('should add a todo to the state', () => {
      const testAction = {
        type: ADD_TODO,
        text: 'Test todo',
      };

      const expectedState = [
        {
          text: testAction.text,
          completed: false,
        },
      ];

      expect(todos([], testAction)).toEqual(expectedState);
    });
  });
});
