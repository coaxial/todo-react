import React from 'react';

import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { QuoteDisplay } from './Quote';

configure({ adapter: new Adapter() });

function setup() {
  const props = {
    isFetching: false,
    quote: 'Test quote',
    author: 'Tester',
  }

  const enzymeWrapper = mount(<QuoteDisplay {...props} />);

  return {
    props,
    enzymeWrapper,
  }
}

describe('components', () => {
  describe('Quote', () => {
    it('should render a placeholder while fetching', () => {
      const testProps = {
        isFetching: true,
        quote: 'Test quote',
        author: 'Tester',
      };
      const enzymeWrapper = mount(<QuoteDisplay {...testProps} />);

      expect(enzymeWrapper.find('p').text()).toBe('Fetching quote...');
    });

    it('should render the quote and its author', () => {
      const { enzymeWrapper } = setup();

      expect(enzymeWrapper.find('p').text()).toBe('Tester: "Test quote"');
    });

    it('should render a placeholder when no quote cached', () => {
      const testProps = {
        isFetching: false,
        quote: '',
        author: '',
      };
      const enzymeWrapper = mount(<QuoteDisplay {...testProps} />);

      expect(enzymeWrapper.find('p').text()).toBe('No quote to show');
    });
  });
});
