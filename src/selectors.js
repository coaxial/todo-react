// @flow
import { createSelector } from 'reselect';
import { filter, intersection, isNil } from 'ramda';

const getStatusFilter = state => state.statusFilter;
const getCategoryFilter = state => state.categoryFilter;
const getTodos = state => state.todos;
const getCategories = state => state.categories;

const visibleTodosByStatusSelector = createSelector(
  [getStatusFilter, getTodos],
  (statusFilter, todos) => {
    switch (statusFilter) {
      case 'ALL':
        return todos;
      case 'COMPLETED':
        return todos.filter(todo => todo.completed);
      case 'PENDING':
        return todos.filter(todo => !todo.completed);
      default:
        return todos;
    }
  }
)

const findTodoFromCategory = ({ categoryFilter, categories, todos }) => {
  const matchValueToFilter = category => category.value === categoryFilter;
  const categoryId = categories.find(matchValueToFilter).id;

  return filter(todo => todo.category === categoryId, todos);
}

const visibleTodosByCategorySelector = createSelector(
  [getCategoryFilter, getCategories, getTodos],
  (categoryFilter, categories, todos) => {
    switch (categoryFilter) {
      case 'ALL':
        return todos;
      default:
        // using default to catch every category, thus allowing the category
        // list to be dynamic
        return findTodoFromCategory({ categoryFilter, categories, todos });
    }
  }
)
export const visibleTodosSelector = createSelector(
  [visibleTodosByStatusSelector, visibleTodosByCategorySelector],
  (statusTodos, categoryTodos) => intersection(statusTodos, categoryTodos)
);

export const categoriesSelector = createSelector(
  [getCategories],
  categories => categories.map(category => ({
    name: category.name,
    value: category.value,
  })),
);

const getCurrentUser = state => state.users.currentUser;

export const isLoggedInSelector = createSelector(
  [getCurrentUser],
  currentUser => !isNil(currentUser.username)
)
