// @flow

import { connect } from 'react-redux';

import type { TState } from './types';
import { addTodo, charTypedTodo, clearAddTodo } from './actions';
import { isLoggedInSelector } from './selectors';
import TodoForm from './TodoForm';

const mapStateToProps = (state: TState) => {
  return {
    wipTodo: state.wipTodo,
    isLoggedIn: isLoggedInSelector(state),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onChange: e => dispatch(charTypedTodo(e.target.value)),
    onClick: (e, text) => {
      e.preventDefault();
      dispatch(addTodo(text));
      dispatch(clearAddTodo());
    },
  }
}

const AddTodo = connect(
  mapStateToProps,
  mapDispatchToProps,
)(TodoForm);

export default AddTodo;
