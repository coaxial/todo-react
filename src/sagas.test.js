import { call, put } from 'redux-saga/effects';

import { quoteApi, fetchQuoteSaga } from './sagas';
import { receiveQuote } from './actions';

describe('sagas', () => {
  const mockQuote = {"quote":"Come celebrate the millennium, with Newmanniun. Newman!","author":"Kramer","season":"8","episode":"20","image":""}

  describe('fetchQuoteSaga', () => {
    const quoteGen = fetchQuoteSaga();

    it('hits the api', () => {
      expect(quoteGen.next().value).toEqual(call(quoteApi));
    });

    it('dispatches a receiveQuote action', () => {
      expect(quoteGen.next(mockQuote).value).toEqual(put(receiveQuote(mockQuote)));
    });

    it('logs to the console on error', () => {
      const error = 'oops';
      expect(quoteGen.throw(error).value).toEqual(call(console.error, error));
    });
  });
});
