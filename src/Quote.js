//@flow

import { connect } from 'react-redux';
import React from 'react';

import type { TQuote, TState } from './types';
import { requestQuote } from './actions';

type qdTypes = {
  isFetching: boolean,
  quote: string,
  author: string,
};

export const QuoteDisplay = ({ isFetching, quote, author }: qdTypes) => {
  if (isFetching) {
    return <p>Fetching quote...</p>;
  }
  if (quote) {
    return <p>{author}: "{quote}"</p>;
  }
  return ( <p>No quote to show</p>);
};

type qcTypes = {
  isFetching: boolean,
  requestQuote: () => mixed,
  currentQuote: TQuote,
}

export const QuoteComponent = ({
  isFetching,
  requestQuote,
  currentQuote
}: qcTypes) => {
  return (
    <div>
      <QuoteDisplay
        quote={currentQuote && currentQuote.quote}
        author={currentQuote && currentQuote.author}
        isFetching={isFetching}
      />
      <button onClick={requestQuote}>Get quote</button>
    </div>
  );
}

const mapStateToProps = (state: TState) => {
  return {
    currentQuote: state.quotes.currentQuote,
    isFetching: state.quotes.isFetching,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    requestQuote: e => {
      dispatch(requestQuote())
    }
  }
}

const Quote = connect(
  mapStateToProps,
  mapDispatchToProps,
)(QuoteComponent);

export default Quote;
