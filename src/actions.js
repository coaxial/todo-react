// @flow
import type {
  TCategory,
  TCategoryFilter,
  TQuote,
  TStatusFilter,
  TTodo,
  TUser,
} from './types';

export const ADD_TODO = 'ADD_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const TOGGLE_TODO = 'TOGGLE_COMPLETED';
export const CHAR_TYPED_TODO = 'CHAR_TYPED_TODO';
export const CLEAR_ADD_TODO = 'CLEAR_ADD_TODO';
export const REQUEST_QUOTE = 'REQUEST_QUOTE';
export const RECEIVE_QUOTE = 'RECEIVE_QUOTE';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAIL = 'USER_LOGIN_FAIL';
export const USER_LOGOUT = 'USER_LOGOUT';
export const CHAR_TYPED_USERNAME = 'CHAR_TYPED_USERNAME';
export const CHAR_TYPED_PASSWORD = 'CHAR_TYPED_PASSWORD';
export const CLEAR_LOGIN_FORM = 'CLEAR_LOGIN_FORM';
export const GET_TODOS = 'GET_TODOS';
export const RECEIVE_TODOS = 'RECEIVE_TODOS';
export const SET_STATUS_FILTER = 'SET_STATUS_FILTER';
export const RECEIVE_CATEGORIES = 'RECEIVE_CATEGORIES';
export const SET_CATEGORY_FILTER = 'SET_CATEGORY_FILTER';
export const CLEAR_TODOS = 'CLEAR_TODOS'
export const CLEAR_CATEGORIES = 'CLEAR_CATEGORIES'
export const PAGE_NOT_FOUND = 'PAGE_NOT_FOUND'

export const receiveCategories = (categories: Array<TCategory>) => ({
  type: RECEIVE_CATEGORIES,
  categories,
});

export const setStatusFilter = (filterName: TStatusFilter) => ({
  type: SET_STATUS_FILTER,
  filterName,
});

export const setCategoryFilter = (filterName: TCategoryFilter) => ({
  type: SET_CATEGORY_FILTER,
  filterName,
});

export const addTodo = (text: string) => ({
  type: ADD_TODO,
  text,
});

export const deleteTodo = (id: number) => ({
  type: DELETE_TODO,
  id,
});

export const toggleTodo = (id: number) => ({
  type: TOGGLE_TODO,
  id,
});

export const charTypedTodo = (string: string) => ({
  type: CHAR_TYPED_TODO,
  string,
});

export const clearAddTodo = () => ({
  type: CLEAR_ADD_TODO,
});

export const requestQuote = () => ({
  type: REQUEST_QUOTE,
});

export const receiveQuote = (quote: TQuote) => ({
  type: RECEIVE_QUOTE,
  quote,
});

export const userLogin = (username: string, password: string) => ({
  type: USER_LOGIN,
  username,
  password,
});

export const userLoginSuccess = (user: TUser) => ({
  type: USER_LOGIN_SUCCESS,
  user,
});

export const userLoginFail = (error: { message: string }) => ({
  type: USER_LOGIN_FAIL,
  error: error.message,
});

export const userLogout = () => ({
  type: USER_LOGOUT,
});

export const charTypedUsername = (username: string) => ({
  type: CHAR_TYPED_USERNAME,
  username,
});

export const charTypedPassword = (password: string) => ({
  type: CHAR_TYPED_PASSWORD,
  password,
});

export const clearLoginForm = () => ({
  type: CLEAR_LOGIN_FORM,
});

export const getTodos = (user: TUser) => ({
  type: GET_TODOS,
  user
});

export const receiveTodos = (todos: Array<TTodo>) => ({
  type: RECEIVE_TODOS,
  todos,
});

export const clearTodos = () => ({
  type: CLEAR_TODOS,
})

export const clearCategories = () => ({
  type: CLEAR_CATEGORIES,
})

export const notFound = (pathname: string) => ({
  type: PAGE_NOT_FOUND,
  pathname,
})
