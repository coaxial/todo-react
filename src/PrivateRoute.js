import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import React from 'react';

import { isLoggedInSelector } from './selectors';

const PrivateRouteComponent = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={props => {
      console.log({ props })
      if (rest.isLoggedIn) {
        return <Component {...props} />
      }
      return <Redirect to={{ pathname: '/' }} />
    }} />
  )
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: isLoggedInSelector(state),
  }
}

const PrivateRoute = connect(
  mapStateToProps,
)(PrivateRouteComponent)

export default PrivateRoute
