// @flow
import { Link } from 'react-router-dom';
import React from 'react';

import type {
  TCategoryFilter,
  TFilters,
  TStatusFilter,
  TTodo,
  TUser,
} from './types';
import DeleteButton from './DeleteButton';

type tlicType = {
  onClick: (e: SyntheticEvent<>) => mixed,
  completed: boolean,
  text: string,
  id: number,
}

const TodoListItemComponent = ({ onClick, completed, text, id }: tlicType) => {
  return (
    <li
      style={{
        textDecoration: completed ? 'line-through' : 'none',
        listStyleType: 'none',
      }}
    >
      <span id={id} onClick={onClick}>
        { text }
      </span>
      <DeleteButton id={id}/>
    </li>
  );
};

type tliType = {
  isLoggedIn: boolean,
  todos: Array<TTodo>,
  onClick: (e: SyntheticEvent<>, id: number) => mixed,
}

const TodoListItem = ({ isLoggedIn, todos, onClick }: tliType) => {
  if (isLoggedIn) {
    if (todos.length === 0) {
      return <p>No todos to display</p>;
    }

    return todos.map((todo, index) => {
      return (
        <TodoListItemComponent
          key={index}
          onClick={e => onClick(e, todo.id)}
          completed={todo.completed}
          text={todo.text}
          id={todo.id}
        />
      );
    });
  }
  return <div />;
};

type flType = {
  isLoggedIn: boolean,
  currentFilter: TStatusFilter | TCategoryFilter,
  filters: Array<TFilters>,
  onFilterClick: (e: SyntheticEvent<>) => mixed,
  isStatusFiltersList?: boolean,
}

const FiltersList = ({
  isLoggedIn,
  currentFilter,
  filters,
  onFilterClick,
  isStatusFiltersList=false,
}: flType) => {
  const filtersToDisplay = [
    {
      name: 'all',
      value: 'ALL',
    },
    ...filters,
  ]
  const defaultStyle = { textDecoration: 'none' }
  const selectedStyle = { textDecoration: 'underline' };

  if (isLoggedIn) {
    return filtersToDisplay.map(filter => {
      const hyperlink = isStatusFiltersList ? `/todos/${filter.name}` : ''

      return (
        <span
          key={filter.value}
        >
          <Link
            to={hyperlink}
            id={filter.value}
            style={currentFilter === filter.value ? selectedStyle : defaultStyle}
            onClick={onFilterClick}
          >
            {filter.name}
          </Link>
          &nbsp;
        </span>
      );
    });
  }
  return <div />
}

const StatusFilters = (ownProps: flType) => {
  return (
    <FiltersList {...ownProps} isStatusFiltersList={true} />
  )
}

type tlTypes = {
  categories: Array<TFilters>,
  currentCategoryFilter: TCategoryFilter,
  currentStatusFilter: TStatusFilter,
  isLoggedIn: boolean,
  onCategoryFilterClick: (e: SyntheticEvent<>) => mixed,
  onRefreshTodosClick: (e: SyntheticEvent<>) => mixed,
  onStatusFilterClick: (e: SyntheticEvent<>) => mixed,
  todos: Array<TTodo>,
  toggleCompleted: (e: SyntheticEvent<>, id: number) => mixed,
  user: TUser,
}

const TodoList = ({
  categories,
  currentCategoryFilter,
  currentStatusFilter,
  isLoggedIn,
  onCategoryFilterClick,
  onRefreshTodosClick,
  onStatusFilterClick,
  todos,
  toggleCompleted,
  user,
}: tlTypes) => {
  return (
    <div>
      <div>
        <StatusFilters
          currentFilter={currentStatusFilter}
          onFilterClick={onStatusFilterClick}
          isLoggedIn={isLoggedIn}
          filters={[
            {
              name: 'completed',
              value: 'COMPLETED',
            },
            {
              name: 'pending',
              value: 'PENDING',
            },
          ]}
        />
      </div>
      <div>
        <FiltersList
          currentFilter={currentCategoryFilter}
          isLoggedIn={isLoggedIn}
          onFilterClick={onCategoryFilterClick}
          filters={categories}
        />
      </div>
      <div>
        <ul>
          <TodoListItem
            isLoggedIn={isLoggedIn}
            todos={todos}
            onClick={toggleCompleted}
          />
        </ul>
      </div>
    </div>
  );
}

export default TodoList;
