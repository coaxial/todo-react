// import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import React from 'react';

import { notFound } from './actions';

class NotFoundComponent extends React.Component {
  componentDidMount() {
    const redirect = history => history.push('/')

    this.props.dispatchNotFound(this.props.history.location.pathname)
    // clearTimeout in componentWillUnmount is unnecessary because it's only
    // used to clear timeouts that didn't trigger yet.
    this.timer = setTimeout(redirect, 3000, this.props.history)
  }

  render() {
    return (
      <div>
        <p>This page doesn't exist, redirecting...</p>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchNotFound: pathname => dispatch(notFound(pathname))
  }
}

const NotFound = withRouter(connect(
  null,
  mapDispatchToProps,
)(NotFoundComponent))

export default NotFound
