import './App.css';

import { Provider } from 'react-redux';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import React, { Component } from 'react';

import { store } from './store';
import Home from './Home';
import NotFound from './NotFound';

class App extends Component {
  store = store

  render() {
    return (
      <Provider store={this.store}>
        <Router>
          <div className="App">
            <Switch>
              <Route path='/' exact component={Home} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
