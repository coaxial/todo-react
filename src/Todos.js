// @flow
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  categoriesSelector,
  isLoggedInSelector,
  visibleTodosSelector,
} from './selectors';
import {
  getTodos,
  setCategoryFilter,
  setStatusFilter,
  toggleTodo,
} from './actions';
import TodoList from './TodoList';

const mapStateToProps = (state, { params }) => {
  return {
    todos: visibleTodosSelector(state),
    user: state.users.currentUser,
    currentStatusFilter: (params && params.statusFilter) || state.statusFilter,
    currentCategoryFilter: state.categoryFilter,
    categories: categoriesSelector(state),
    isLoggedIn: isLoggedInSelector(state),
  }
};

const mapDispatchToProps = dispatch => {
  return {
    toggleCompleted: (e, id) => dispatch(toggleTodo(id)),
    onRefreshTodosClick: (e, user) => dispatch(getTodos(user)),
    onStatusFilterClick: e => dispatch(setStatusFilter(e.target.id)),
    onCategoryFilterClick: e => dispatch(setCategoryFilter(e.target.id)),
  }
}

const Todos = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(TodoList));

export default Todos;
