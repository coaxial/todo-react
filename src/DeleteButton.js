// @flow
import { connect } from 'react-redux';
import React from 'react';

import type { TState } from './types';
import { deleteTodo } from './actions';

type dbcType = {
  onClick: (e: SyntheticEvent<>, id: number) => mixed,
  id: number,
}

const DeleteButtonComponent = ({ onClick, id }: dbcType) => {
  return (
    <button
      onClick={e => onClick(e, id)}
      style={{ marginLeft: '1rem' }}
    >
      x
    </button>
  );
}

const mapStateToProps = (state: TState, { id }) => {
  return ({
    id,
  });
}

const mapDispatchToProps = dispatch => {
  return ({
    onClick: (e, id) => dispatch(deleteTodo(id)),
  });
}

const DeleteButton = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeleteButtonComponent);

export default DeleteButton;
