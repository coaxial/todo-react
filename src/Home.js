import React from 'react';

import AddTodo from './AddTodo';
import Auth from './Auth';
import PrivateRoute from './PrivateRoute';
import Quote from './Quote';
import Todos from './Todos';

const Home = () => {
  return (
    <div>
      <Auth />
      <AddTodo />
      <PrivateRoute path="/todos/:statusFilter" component={Todos} />
      <Quote />
    </div>
  )
}

export default Home
