// @flow

import { ADD_TODO, DELETE_TODO, RECEIVE_TODOS, TOGGLE_TODO } from './actions';

export type TId = number

export type TQuote = {
  quote: string,
  author: string,
  season: string,
  episode: string,
  image: string,
}

export type TStateQuotes = {
  currentQuote: TQuote | {},
  isFetching: boolean,
}

export type TTodo = {
  id: TId,
  text: string,
  completed: boolean,
  category: number,
}

export type TUser = {
  username: string,
  displayName: string,
  todoIds: Array<number>,
}

export type TStateUsers = {
  isFetching: boolean,
  currentUser: TUser | {},
  lastError: string,
  username: string,
  password: string,
}

export type TStateAuthForm = {
  username: string,
  password: string,
}

export type TFilters = {
  name: string,
  value: string,
}

export type TStatusFilter = 'ALL' | 'COMPLETED' | 'PENDING'

export type TCategoryFilter = string

export type TCategory = {
  id: TId,
  name: string,
  value: TCategoryFilter,
}

export type TCategories = Array<TCategory>

export type TStateWipTodo = string

export type TStateCategoryFilter = string

export type TStateCategories = TCategories

export type TState = {
  +todos: Array<TTodo>,
  +wipTodo: TStateWipTodo,
  +quotes: TStateQuotes,
  +users: TStateUsers,
  +authForm: TStateAuthForm,
  +statusFilter: TStatusFilter,
  +categories: TStateCategories,
  +categoryFilter: TStateCategoryFilter,
}

export type TFilterAction = {
  type: string,
  filterName: TCategoryFilter | TStatusFilter
}

export type TTodosAction =
  | { +type: 'ADD_TODO', text: $PropertyType<TTodo, 'text'> }
  | { +type: 'DELETE_TODO', id: $PropertyType<TTodo, 'id'> }
  | { +type: 'TOGGLE_COMPLETED', id: $PropertyType<TTodo, 'id'> }
  | { +type: 'RECEIVE_TODOS', todos: Array<TTodo> }
  | { +type: 'GET_TODOS', user: TUser }
  | { +type: 'CLEAR_TODOS' }

export type TWipTodoAction =
  | { +type: 'CHAR_TYPED_TODO', string: TStateWipTodo }
  | { +type: 'CLEAR_ADD_TODO', string: TStateWipTodo }

export type TAuthFormAction =
  | { +type: 'CHAR_TYPED_USERNAME', username: string }
  | { +type: 'CHAR_TYPED_PASSWORD', password: string }
  | { +type: 'CLEAR_LOGIN_FORM' }

export type TUsersAction =
  | { +type: 'USER_LOGIN', username: string, password: string }
  | { +type: 'USER_LOGIN_SUCCESS', user: TUser }
  | { +type: 'USER_LOGIN_FAIL', error: string }

export type TQuotesAction =
  | { +type: 'REQUEST_QUOTE' }
  | { +type: 'RECEIVE_QUOTE', quote: TQuote }

export type TCategoriesAction =
  | { +type: 'RECEIVE_CATEGORIES', categories: TCategories }
  | { +type: 'CLEAR_CATEGORIES' }
