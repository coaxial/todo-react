import createSagaMiddlware from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';

import rootSaga from './sagas';
import todoApp from './reducers';

const logger = createLogger({ collapsed: true })
const sagas = createSagaMiddlware()

export const store = createStore(
  todoApp,
  composeWithDevTools(
    applyMiddleware(
      logger,
      sagas,
    ),
  ),
)

sagas.run(rootSaga)
