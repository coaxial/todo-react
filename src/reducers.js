//@flow
import { clone } from 'ramda';
import { combineReducers } from 'redux';

import {
  ADD_TODO,
  CHAR_TYPED_PASSWORD,
  CHAR_TYPED_TODO,
  CHAR_TYPED_USERNAME,
  CLEAR_ADD_TODO,
  CLEAR_CATEGORIES,
  CLEAR_LOGIN_FORM,
  CLEAR_TODOS,
  DELETE_TODO,
  RECEIVE_CATEGORIES,
  RECEIVE_QUOTE,
  RECEIVE_TODOS,
  REQUEST_QUOTE,
  SET_CATEGORY_FILTER,
  SET_STATUS_FILTER,
  TOGGLE_TODO,
  USER_LOGIN,
  USER_LOGIN_FAIL,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
} from './actions';
import type {
  TAuthFormAction,
  TCategoriesAction,
  TCategoryFilter,
  TFilterAction,
  TQuotesAction,
  TStateAuthForm,
  TStateCategories,
  TStateQuotes,
  TStateUsers,
  TStateWipTodo,
  TStatusFilter,
  TTodo,
  TTodosAction,
  TUsersAction,
  TWipTodoAction,
} from './types';

export function categoryFilter(
  state: TCategoryFilter = 'ALL',
  action: TFilterAction,
) {
  switch (action.type) {
    case SET_CATEGORY_FILTER:
      return action.filterName;
    default:
      return state;
  }
}

export function statusFilter(
  state: TStatusFilter = 'ALL',
  action: TFilterAction,
) {
  switch (action.type) {
    case SET_STATUS_FILTER:
      return action.filterName;
    default:
      return state;
  }
}

export function todos(
  state: Array<TTodo> = [],
  action: TTodosAction,
) {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        {
          text: action.text,
          completed: false,
        },
      ];

    case DELETE_TODO:
      if (action.id >= 0) {
        const stateCopy = clone(state)
        stateCopy[action.id] = undefined;
        return stateCopy.filter(el => el !== undefined);
      }
      return state;

    case TOGGLE_TODO:
      return state.map((todo, index) => {
        if (index === action.id) {
          return ({
            ...todo,
            completed: !todo.completed,
          });
        }
        return todo;
      })

    case RECEIVE_TODOS:
      return action.todos;

    case CLEAR_TODOS:
      return []

    default:
      return state;
  }
}

export function wipTodo(
  state: TStateWipTodo = '',
  action: TWipTodoAction,
) {
  switch (action.type) {
    case CHAR_TYPED_TODO:
      return action.string;

    case CLEAR_ADD_TODO:
      return '';

    default:
      return state;
  }
}

export function authForm(
  state: TStateAuthForm = {
    username: '',
    password: '',
  },
  action: TAuthFormAction,
) {
  switch(action.type) {
    case CHAR_TYPED_USERNAME:
      return ({
        ...state,
        username: action.username,
      });

    case CHAR_TYPED_PASSWORD:
      return ({
        ...state,
        password: action.password,
      });

    case CLEAR_LOGIN_FORM:
      return ({
        ...state,
        username: '',
        password: '',
      });

    default:
      return state;
  }
}

export function users(
  state: TStateUsers = {
    isFetching: false,
    currentUser: {},
    lastError: '',
    username: '',
    password: '',
  },
  action: TUsersAction,
) {
  switch(action.type) {
    case USER_LOGIN:
      return ({
        ...state,
        isFetching: true,
        username: action.username,
        password: action.password,
      });

    case USER_LOGIN_SUCCESS:
      return ({
        ...state,
        isFetching: false,
        currentUser: action.user,
        username: '',
        password: '',
        lastError: '',
      });

    case USER_LOGIN_FAIL:
      return ({
        ...state,
        isFetching: false,
        lastError: action.error,
        username: '',
        password: '',
      });

    case USER_LOGOUT:
      return ({
        ...state,
        currentUser: [],
      });

    default:
      return state;
  }
}

export function quotes(
  state: TStateQuotes = {
    currentQuote: {},
    isFetching: false,
  },
  action: TQuotesAction,
) {
  switch (action.type) {
    case REQUEST_QUOTE:
      return ({
        ...state,
        isFetching: true,
      });
    case RECEIVE_QUOTE:
      return ({
        ...state,
        isFetching: false,
        currentQuote: action.quote,
      });
    default:
      return state;
  }
}

export function categories(
  state: TStateCategories = [],
  action: TCategoriesAction,
) {
  switch (action.type) {
    case RECEIVE_CATEGORIES:
      return action.categories;

    case CLEAR_CATEGORIES:
      return [];

    default:
      return state;
  }
}

// root reducer
const todoApp = combineReducers({
  todos,
  wipTodo,
  quotes,
  users,
  authForm,
  statusFilter,
  categories,
  categoryFilter,
});

export default todoApp;
